" Commencer par installer vundle :
"   mkdir -p ~/.vim/bundle
"   cd ~/.vim/bundle
"   git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
"
"   Installer fzf (pour l'ouverture rapide de fichier entre autre avec C-p)
"   git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
"   ~/.fzf/install
"
"   et copier ce fichier dans ~/.vimrc
"
"   Plugin hors Vundle :
"   vim-anywhere (https://github.com/qutebrowser/qutebrowser/)
"   Installation : curl -fsSL https://raw.github.com/cknadler/vim-anywhere/master/install | bash
"   Update : ~/.vim-anywhere/update
"   Uninstall : ~/.vim-anywhere/uninstall
"   I3 integrating : echo 'bindsym $mod+Shift+v exec ~/.vim-anywhere/bin/run' >> ~/.i3/config

set nocompatible                     " be iMproved, required
filetype off                         " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'        " Gestionnaire des plugins
Plugin 'davewongillies/vim-gradle'   " Coloration syntaxique pour travailler avec gradle
Plugin 'scrooloose/nerdtree'         " Arborescence (toggle avec <C-n>
" Plugin 'airodactyl/neovim-ranger'         " Arborescence (toggle avec <C-n>
Plugin 'PotatoesMaster/i3-vim-syntax' " Coloration syntaxique pour les fichier de conf i3wm
Plugin 'Xuyuanp/nerdtree-git-plugin' " Affiche des icones sur les fichiers dans nerdtree
Plugin 'pangloss/vim-javascript'     " Coloration syntaxique pour le JS
Plugin 'Yggdroot/indentLine'         " Affiche des lignes verticales qui suivent l'indentation (toggle <C-i>
Plugin 'itchyny/lightline.vim'       " Barre de status en bas
Plugin 'itchyny/vim-gitbranch'       " Sert juste a retourner le nom de la branche git pour lightline
Plugin 'junegunn/fzf.vim'            " Ouverture rapide de fichier
Plugin 'ap/vim-buftabline'           " Ligne des buffers en haut (se déplacer dans les buffers : <S-Right> <S-Left>)
Plugin 'airblade/vim-gitgutter'      " Identifie les lignes ajoutées, modifiées ou supprimées (par rapport a git)
Plugin 'StanAngeloff/php.vim'        " Coloration syntaxique pour PHP
" Plugin 'artur-shaik/vim-javacomplete2' " Autocompletion pour Java
Plugin 'rhysd/vim-grammarous'
Plugin 'tpope/vim-fugitive'
Plugin 'godlygeek/tabular'
" Plugin 'Valloric/YouCompleteMe'      " Autocompletion
" Plugin 'ntpeters/vim-better-whitespace' " Surligne les espaces de fin de ligne

" All of your Plugins must be added before the following line
call vundle#end()                    " required
filetype plugin indent on            " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" https://github.com/junegunn/fzf
set rtp+=~/.fzf

set number relativenumber           " -- Numéro du ligne

" -- Recherche
set ignorecase                      " Ignore la casse lors d’une recherche
set smartcase                       " Si une recherche contient une majuscule, re-active la ensibilite a la casse
set incsearch                       " Surligne les resultats de recherche pendant la saisie
set hlsearch                        " Surligne les resultats de recherche
syntax on

set mouse=a


let g:indentLine_char = '┆'
let g:ycm_min_num_of_chars_for_completion = 1

" Bind toggle NerdTree
map <C-n> :NERDTreeToggle<CR>

" Bind "set hlSearch pour le surlignage des recherches
map <C-h> :set hlsearch!<CR>

" Bind :set list! (afficher les caractères invisible)
map <C-S-l> :set list! listchars=tab:▸·,trail:·<CR>

" Bind :Files (fzf vim plugin)
map <C-p> :Files<CR>
map <C-l> :Lines<CR>

" Bind :Marks (fzf vim plugin)
" map <C-m> :Marks<CR>

" Bind :IndentLinesToggle (indentLine plugin)
map <C-i> :IndentLinesToggle<CR>

map <S-Left> :bprev<CR>
map <S-Right> :bnext<CR>

" map <S-h> :bprev<CR>
" map <S-l> :bnext<CR>

map <C-g> :GitGutter<CR>
" map <C-g> :GitGutterLineHighlightsToggle<CR>
map <A-PageUp> :GitGutterPrevHunk<CR>
map <A-PageDown> :GitGutterNextHunk<CR>

" configuration nerdtree-git
" let g:NERDTreeIndicatorMapCustom = {
    " \ "Modified"  : "✹",
    " \ "Staged"    : "✚",
    " \ "Untracked" : "✭",
    " \ "Renamed"   : "➜",
    " \ "Unmerged"  : "═",
    " \ "Deleted"   : "✖",
    " \ "Dirty"     : "✗",
    " \ "Clean"     : "✔︎",
    " \ 'Ignored'   : '☒',
    " \ "Unknown"   : "?"
    " \ }

" Status line (lightline.vim)
set laststatus=2
set noshowmode
let g:lightline = {
    \ 'colorscheme': 'wombat',
    \ 'active': {
    \   'left': [ [ 'mode', 'paste' ],
    \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
    \ },
    \ 'component_function': {
    \   'gitbranch': 'gitbranch#name'
    \ },
\ }

" Show (partial) command in status line.
set showcmd

" enable setting title
set title
" configure title to look like: Vim /path/to/file
" set titlestring=\ %-25.55F\ %a%r%m titlelen=70

" virtual tabstops using spaces
" HACK: Set the TABSIZE environment variable to
"       custom the shiftwidth and tabstop settings
"       eg. `TABSIZE=2 vim `
" WARNING: Only work for TABSIZE=2 (vimrc cannot convert String to int)
if $TABSIZE == 2
  set shiftwidth=2
  set tabstop=2
else
  set shiftwidth=4
  set tabstop=4
endif
set expandtab

" allow toggling between local and default mode
function TabToggle()
  if &expandtab
    set shiftwidth=4
    set tabstop=4
    set noexpandtab
  else
    set shiftwidth=4
    set softtabstop=4
    set expandtab
  endif
endfunction
nmap <F9> mz:execute TabToggle()<CR>'z

" No new line
set nofixendofline

" Compter le nombre de match de la dernière recherche
nnoremap <C-s> <C-O>:%s///gn<CR>``

" Activer l'autocompletion pour les fichiers JAVA
" autocmd FileType java setlocal omnifunc=javacomplete#Complete

" Omnicomplete - don't use this if you need <C-o> (useful...I prefer <Esc>)
inoremap <silent> <C-o> <C-x><C-o>
" Ne pas ouvrir de window preview lorsqu'on demande l'omnicompletion
set completeopt-=preview

" Désactiver l'insertion automatique de commentaire
" https://vim.fandom.com/wiki/Disable_automatic_comment_insertion
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

