# Installation de TPM (Gestionnaire de plugin) :
# git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
# tmux source ~/.tmux.conf
#
# To update the plugins, Press prefix + I
# More info : https://github.com/tmux-plugins/tpm


# List of plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
# set -g @plugin 'tmux-plugins/tmux-resurrect'
# set -g @plugin 'tmux-plugins/tmux-continuum'
set -g @plugin 'tmux-plugins/tmux-sidebar'

# 0 is too far from ² ;)
set -g base-index 1

# Vim style pane selection
bind h select-pane -L
bind j select-pane -D 
bind k select-pane -U
bind l select-pane -R

# reload config file (change file location to your the tmux.conf you want to use)
bind-key r source-file ~/.tmux.conf

# remap prefix from 'C-b' to 'C-q'
unbind C-b
set-option -g prefix C-q
bind-key C-a send-prefix

# Automatic restore (https://github.com/tmux-plugins/tmux-continuum)
set -g @continuum-restore 'on'

# Enable automatic start with systemd (https://github.com/tmux-plugins/tmux-continuum/blob/master/docs/automatic_start.md)
set -g @continuum-boot 'on'

# Disable mouse mode (tmux 2.1 and above)
set -g mouse off

# Use vi key in buffer
set -g mode-keys vi
# Use vi key bindings in the status line
set -g status-keys vi

# By default, up to 2000 lines are kept
set -g history-limit 10000

# reset the terminal history
bind-key k clear-history

# change copy mode key binding
bind-key C copy-mode

# don't rename windows automatically
set-option -g allow-rename off

# Tmux Vim-bindings for copying into tmux buffer (https://www.rushiagr.com/blog/2016/06/16/everything-you-need-to-know-about-tmux-copy-pasting-ubuntu/)
bind P paste-buffer
bind-key -T copy-mode-vi v send-keys -X begin-selection
bind-key -T copy-mode-vi y send-keys -X copy-selection
bind-key -T copy-mode-vi r send-keys -X rectangle-toggle

# Copy from tmux buffer to system buffer (clipboard) (https://www.freecodecamp.org/news/tmux-in-practice-integration-with-system-clipboard-bcd72c62ff7b/)
bind -T copy-mode-vi Enter send-keys -X copy-pipe-and-cancel "xclip -i -f -selection primary | xclip -i -selection clipboard"

# Restoring neovim sessions
set -g @resurrect-strategy-nvim 'session'

# THEME
set -g status-bg black
set -g status-fg white
# set -g window-status-current-bg-style white
# set -g window-status-current-fg-style black
# set -g window-status-current-attr-style bold
set -g status-interval 60
set -g status-left-length 40
set -g status-left '#[fg=green](#S) #(whoami)'
set -g status-right '#[fg=yellow]#(cut -d " " -f 1-3 /proc/loadavg)#[default] #[fg=white]%D %H:%M#[default]'

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run -b '~/.tmux/plugins/tpm/tpm'
