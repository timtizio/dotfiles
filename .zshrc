###########
# Antigen #
###########

# Installer Antigen (gestionnaire de plugin)
# curl -L git.io/antigen > antigen.zsh

source $HOME/antigen.zsh
antigen use oh-my-zsh

antigen bundle git
# antigen bundle sudo
antigen bundle colored-man-pages
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-autosuggestions
antigen bundle smallhadroncollider/antigen-git-rebase
antigen bundle popstas/zsh-command-time

# workaround for https://github.com/zsh-users/antigen/issues/675
THEME=win0err/aphrodite-terminal-theme
antigen list | grep $THEME; if [ $? -ne 0 ]; then antigen theme $THEME; fi

antigen apply

######################
# User configuration #
######################

bindkey '^ ' autosuggest-accept # (Ctrl + Espace)

# On garde 1 million de lignes dans l'historique
HISTFILE="$HOME/.zsh_history"
HISTSIZE=1000000
SAVEHIST=1000000

export EDITOR=vim

# Color for aphrodite theme
export TERM="xterm-256color"

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# Utiliser la touche Caps_Lock comme la touche control
setxkbmap -layout fr -option ctrl:nocaps

# Ajout des binaires du sdk android
export PATH=$PATH:$HOME/Android/Sdk/platform-tools/:$HOME/Android/Sdk/tools/bin/

# Permet de définir la variable JAVA_HOME
# source $HOME/bin/java.sh

##############
# User alias #
##############

source $HOME/bin/myaliases.sh
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
