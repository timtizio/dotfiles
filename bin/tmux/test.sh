#! /bin/sh
session="blog"
window="$session"

if [ $(tmux attach -t "$session") ]; then
  exit 0
fi

cd $HOME/workspace
tmux new-session -d -s "$session"

tmux split-window -t "$window"
tmux split-window -t "$window"
tmux select-layout -t "$window" main-vertical

tmux send-keys -t "$window.1" "kak" C-m
tmux send-keys -t "$window.2" "hugo server --navigateToChanged --disableFastRender" C-m
tmux send-keys -t "$window.3" "git status" C-m

tmux attach -t "$session"
