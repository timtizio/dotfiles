#! /bin/sh
session="hscd2"
workpath="$HOME/workspace/multimed/android/hscd2"

if [ $(tmux attach -t "$session") ]; then
  exit 0
fi

cd $workpath
tmux new-session -d -s "$session"

tmux rename-window "workspace"
tmux split-window -h
tmux send-keys -t "workspace.0" "adb devices" C-m

tmux new-window -n "dev"
tmux send-keys -t "dev.0" "vim +NERDTree" C-m

tmux new-window -n "git"
tmux send-keys -t "git.0" "git status" C-m

tmux attach -t "$session"

tmux select-window -t "workspace"
