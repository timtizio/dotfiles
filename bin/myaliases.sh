##############
# User alias #
##############

alias glg='git lg'
alias grdl='./gradlew assembleDebug'
alias gitnp='git --no-pager'
alias gnp='git --no-pager'

alias logcat='adb logcat -c && adb logcat'

# alias adb=adb+

alias rmspace="rename 's/[[:blank:]]/_/g'"

# kill all gradle process
alias stopgradle="pkill -f '.*GradleDaemon.*'"

alias ls="ls --color"

# alias vi="vim"
alias vim="nvim"
